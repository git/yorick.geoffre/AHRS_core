#!/bin/bash

# Update package lists
sudo apt-get update

# Install necessary packages
sudo apt-get install -y \
    build-essential \
    cmake \
    pkg-config \
    libopencv-dev \
    libgl1-mesa-dev \
    libglu1-mesa-dev \
    freeglut3-dev \
    libfreetype6-dev

# Download and install GLFW if not installed
if ! command -v glfw &> /dev/null; then
    git clone https://github.com/glfw/glfw.git
    cd glfw
    mkdir build && cd build
    cmake .. && make
    sudo make install
    cd ../..
    rm -rf glfw
fi

# Download and install GLAD if not installed
if ! command -v glad &> /dev/null; then
    git clone https://github.com/Dav1dde/glad.git
    cd glad
    mkdir build && cd build
    cmake .. && make
    sudo make install
    cd ../..
    rm -rf glad
fi

# Download and install GLEW if not installed
if ! command -v glewinfo &> /dev/null; then
    wget https://github.com/nigels-com/glew/releases/download/glew-2.2.0/glew-2.2.0.tgz
    tar -xvf glew-2.2.0.tgz
    cd glew-2.2.0/build
    cmake ./cmake && make
    sudo make install
    cd ../..
    rm -rf glew-2.2.0
    rm glew-2.2.0.tgz
fi

echo "All dependencies have been installed."